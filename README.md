### Pasos para la creación de un proyecto de **AutoMATE**
### =====================================================

1. Como Pre requisito debemos tener instalado:
- JDK version 1.8
- Maven version 3.5

Para comprobar las instalaciones, ejecutar en consola:
```
java	--version
mvn	--version
```

2. Descargar el instalador para su sistema operativo

[AutoMATE - Windows](https://gitlab.com/AutoMATE-HA/AutoMATEDependecies/raw/master/AutoMATEDependecies.zip)

[AutoMATE - Linux](https://gitlab.com/AutoMATE-HA/AutoMATEDependecies/raw/master/AutoMATEDependecies.tar.gz)


3. Descomprimir el paquete de AutoMATE y ejecutar el instaldor:

[Windows] = ```automate.bat```
[Linux] = ```sh automate.sh```

# Y WALLA! ahora ya podes comenzar a automatizar y correr test cases

Para instalar dependencias para Linux:

Para instalar Java:
```$ sudo add-apt-repository ppa:webupd8team/java```
```$ sudo apt update```
```$ sudo apt install oracle-java8-installer```

Configurar variables de entorno:
```$ sudo apt install oracle-java8-set-default```

Para instalar Maven:
```$ sudo apt-get install maven```

Para instalar Curl:
```$ sudo apt-get install curl```



