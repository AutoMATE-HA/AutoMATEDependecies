@echo off

java -jar automate-easy-install.jar
set /p projectName=<pn.txt
call mvn clean install -DskipTests -f %projectName%\pom.xml
del pn.txt
set banner=type banner.txt
%banner%
del banner.txt
